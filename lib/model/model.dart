import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:sqfentity/sqfentity.dart';
import 'package:sqfentity_gen/sqfentity_gen.dart';
part 'model.g.dart';

const tableInvoice = SqfEntityTable (
  tableName: 'invoice',
  primaryKeyName: 'id',
  primaryKeyType: PrimaryKeyType.integer_auto_incremental,
  useSoftDeleting: false,
  modelName: null,
  fields: [
    SqfEntityField('description', DbType.text),
    SqfEntityField('time', DbType.datetime),
    SqfEntityFieldRelationship(
      parentTable: tableJourney,
      deleteRule: DeleteRule.CASCADE
    ),
    SqfEntityField('imageUrl', DbType.text)
  ]
);

const tableJourney = SqfEntityTable (
  tableName: 'journey',
  primaryKeyName: 'id',
  primaryKeyType: PrimaryKeyType.integer_auto_incremental,
  useSoftDeleting: false,
  modelName: null,
  fields: [
    SqfEntityField('description', DbType.text),
    SqfEntityField('date', DbType.date),
  ]
);

const seqIdentity = SqfEntitySequence(
  sequenceName: 'identity',
);

@SqfEntityBuilder(myDbModel)
const myDbModel = SqfEntityModel(
    modelName: 'LazyModel', // optional
    databaseName: 'lazyORM.db',

    databaseTables: [tableJourney, tableInvoice],

    sequences: [seqIdentity],
    bundledDatabasePath: null
);
