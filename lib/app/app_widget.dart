import 'package:flutter/material.dart';
import 'package:lazy/app/pages/home/home_module.dart';

var cores = [
  Color(0xFFFD5C6F), // #FD5C6F 0
  Color(0xFFFF8F71), // #FF8F71 1
  Color(0xFFFFBE73), // #FFBE73 2
  Color(0xFFF9EE80), // #F9EE80 3
  Color(0xFF2EDDDA) // #2EDDDA  4
];

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Slidy',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: cores[4],
        accentColor: cores[4]
      ),
      home: HomeModule(),
    );
  }
}
