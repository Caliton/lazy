import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:lazy/model/model.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';

class SharedBloc extends BlocBase {
  List<Invoice> invoiceList = [];

  BehaviorSubject<List<Invoice>> _listController;
  
  SharedBloc() {
    _listController = BehaviorSubject.seeded(invoiceList);
  }

  Observable<List<Invoice>> get listOut => _listController.stream;

  expulgar() {
    _listController.add([]);
  }
  addInvoice(Invoice model) {
    print('EAEAE BANDO DE SAFADO SOU O ADDINVOICE');
    print(model);
    invoiceList.add(model);
    _listController.add(invoiceList);
  }

  @override
  void dispose() {
    _listController.close();
    super.dispose();
  }
}
