import 'package:flutter/material.dart';
import 'package:lazy/app/shared/blocs/shared_bloc.dart';
import 'package:lazy/model/model.dart';

import '../../app_module.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final blocInvoice = AppModule.to.getBloc<SharedBloc>();

  bool _pinned = true;
  bool _snap = false;
  bool _floating = false;
  bool cachorro = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            actions: <Widget>[
              ButtonBar(
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.info),
                    onPressed: () {},
                    tooltip: 'Info',
                    color: Colors.black54,
                  )
                ],
              )
            ],
            automaticallyImplyLeading: false,
            shape: RoundedRectangleBorder(
                borderRadius:
                    BorderRadius.vertical(bottom: Radius.circular(30))),
            pinned: this._pinned,
            floating: this._floating,
            snap: this._snap,
            expandedHeight: 160.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Transform.translate(
                offset: Offset(-50.0, 20.0),
                child: ListTile(
                  leading: CircleAvatar(
                    child: Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.transparent,
                          image: DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage("assets/logo-app.png"))),
                    ),
                  ),
                  title: Text(
                    "Papéis Lixo",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15.0,
                        letterSpacing: 2.0,
                        shadows: [
                          Shadow(
                              blurRadius: 20.0,
                              color: Colors.black,
                              offset: Offset(0.0, 0.0)),
                        ]),
                  ),
                  subtitle: Text(
                    "para quem odeia esses papéis <3",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 10.0,
                      letterSpacing: 1.0,
                    ),
                  ),
                ),
              ),
              background: Container(
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.vertical(bottom: Radius.circular(30))),
                  child:
                      Image.network("https://picsum.photos/800/400?random") !=
                              null
                          ? Image.network(
                              "https://picsum.photos/800/400?random",
                              fit: BoxFit.cover)
                          : AssetImage('assets/bottom.jpg')),
            ),
          ),
          SliverFillRemaining(
              child: Container(
            child: Center(
                child: StreamBuilder<List<Invoice>>(
              stream: blocInvoice.listOut,
              builder: (_, snapshot) {
                // Loading....
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                final List<Invoice> models = snapshot.data;
                Future<List>.sync(() { return Invoice().select().toList();}).then((List list) {
                  print('OI');
                  print(list);

                });
                
                if (models.length == 0) {
                  return emptyState(title: 'Ops!', message: "Ainda não temos nada registrado :)");
                }
                return ListView.separated(
                    itemCount: models.length,
                    itemBuilder: (_, index) {
                      print(models.length);
                      print(models[index]);
                      return ListTile(
                        leading: Container(
                          height: 100,
                          width: 100,
                          child: Text('A') 
                        ),
                        title: Text(models[index].description), 
                        subtitle: Text(models[index].time.toIso8601String()),
                      );
                    },
                    separatorBuilder: (_, index) {
                      return Divider();
                    });
              },
            )),
          )),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.photo_camera,
          color: Colors.white,
        ),
        onPressed: () {

          final model = Invoice();
          model.description = 'Invoice 1';
          model.time = DateTime.now();
          model.imageUrl = 'asdf';
          model.save();
          return model;
          Future<Invoice>.sync(() {

          }).then((vish) {
              AppModule.to.bloc<SharedBloc>().addInvoice(model);
          });
        },
      ),
    );
  }

  Widget emptyState({
    title: '',
    message: '',
  }) {
    return Material(
      borderRadius: BorderRadius.circular(16),
      elevation: 16,
      color: Theme.of(context).cardColor.withOpacity(.95),
      shadowColor: Theme.of(context).accentColor.withOpacity(.5),
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(title, style: Theme.of(context).textTheme.headline),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(message),
            )
          ],
        ),
      ),
    );
  }
}
